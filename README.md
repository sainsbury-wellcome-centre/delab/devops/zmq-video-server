# zmq-video-server

This service runs on a computer to record a video to disk using opencv and zmq subscriber to control when to start and stop videos.

There are 4 components to this:

+ A file watcher that copies finished session videos to the file server.
+ A zmq listener state machine that conrols start/stopping and getting trial info
+ A video_capture thread that takes a filename and a shared object for control
+ A subtitle writer thread that takes a filename and a shared object for control

# required package
If you are running on UDP messaging mode, the only required message is: `opencv`, you may just do:
+ `sudo apt install python3-pip`
+ `pip install opencv-python`
+ `sudo apt-get install ffmpeg x264 libx264-dev`

# run
## method 1:
`python ~/zmq-video-server/src/video_server.py`

## method 2:
`./start_video_server.sh`