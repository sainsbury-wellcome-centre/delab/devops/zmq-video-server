import configparser
import os
import time
import sys
import threading
import subprocess
import pyautogui
import traceback
import Xlib.threaded
import udp_messager as udp_mess
import videocapture as vid

pyautogui.FAILSAFE = False
filename = ''
kill_video_flag = False
current_subjid = 'JLI-T-0001'

class vid_status(object):
    current_status = ''
    current_msg = ''
    updated = False
    recording = False
    test = False

    def __init__(self):
        self.current_status = ''

status = vid_status()

if __name__ == "__main__":
    ini_path = os.path.join(os.path.expanduser('~'),'.dbconf')
    config = configparser.ConfigParser()
    config.sections()
    config.read(ini_path)

    host = config['vid']['host']
    port = config['vid']['port']
    method = config['vid']['method']
    if config.has_option('vid', 'flip'):
        flip = (config['vid']['flip'] == '1')
    else:
        flip = False


    if method == 'UDP':
        mess = udp_mess.udp_messager()
    elif method == 'ZMQ':
        mess = zmq_mess.zmq_messager()
    mess.setTimeout(900) #max for 15min 
    mess.getConnection(host,port)
    video_recording = vid.video_recorder(flip)
    try:
        trig_mess=threading.Thread(target=mess.read_and_update_status,args=(status,),name='triggering_message')
        trig_mess.start()
        while True:
            if status.updated:
                # set back to False once update received
                status.updated = False 
                if status.current_status == None:
                    # start over
                    # this is a timeout message, pro something bad, need to stop video recordingm
                    if status.recording:
                        print('message timeout, bpod is bad? stopping video recording')
                        status.recording = False
                        status.current_status = 'ended'
                        kill_video_flag = True
                        print("stop recording for " + filename)
                        if not status.test:
                            target_path = os.path.join(os.path.expanduser('~'),'completed',current_subjid)
                            if not os.path.exists(target_path):
                                mkdir_cmd = 'mkdir ' + target_path
                                os.system(mkdir_cmd)
                            move_cmd = "mv %s %s" % (filename,target_path)
                            print(move_cmd)
                            os.system(move_cmd)
                            subprocess.Popen("rsync -av --remove-source-files /home/delab/completed/ videofs:video/",shell=True)
                elif status.current_status == 'running':
                    # recording is running, pass
                    pass
                elif status.current_status == 'ended':
                    pass
                elif status.current_status == 'start' and status.recording == False:
                    # start the recording for a new session
                    if status.test:
                        filename = "rig_test"
                        t_start = time.time()
                    else:
                        filename =  "%s_%s_%s.mp4" % (status.current_msg['subjid'], str(status.current_msg['sessid']),status.current_msg['ts'])
                        filename = os.path.join(os.path.expanduser('~'),'in_process',filename)
                        current_subjid = status.current_msg['subjid']
                    print("start recording for " + filename)
                    mess.sessid = status.current_msg['sessid']
                    video_recording.init_recording(fname = filename,testing = status.test)
                    status.recording = True # this is a flag to start acquiring video recording
                    status.current_status = 'running'
                    kill_video_flag = False
                    # press shift to keep screen alive
                    try:
                        pyautogui.press('shift') #keep screen alive if received useful message
                    except:
                        print('error pressing shift')
                        traceback.print_exc()
                elif status.current_status == 'start' and status.recording == True:
                    print('recording already started')
                elif status.current_status == 'stop' and status.recording == True:
                    # recording is already started, here the session will be stopped, and here will process for moving folder from in_process to completed
                    # move recording file to competed folder!
                    status.recording = False
                    status.current_status = 'ended'
                    kill_video_flag = True
                    print("stop recording for " + filename)
                    if not status.test:
                        target_path = os.path.join(os.path.expanduser('~'),'completed',current_subjid)
                        if not os.path.exists(target_path):
                            mkdir_cmd = 'mkdir ' + target_path
                            os.system(mkdir_cmd)
                        move_cmd = "mv %s %s" % (filename,target_path)
                        print(move_cmd)
                        os.system(move_cmd)
                        subprocess.Popen("rsync -av --remove-source-files /home/delab/completed/ videofs:video/",shell=True)
                elif status.current_status == 'restart' and status.recording == True:
                    # recording for a wrong sessid, have message lost, the current recording was already stopped, need to be restarted
                    # move recording file to competed folder!
                    print('restarting new recording, create a new thread for video recording')
                    status.current_status = 'ended'
                    kill_video_flag = True
                    status.recording = False
                    # kill video recording here
                    video_recording.destroy_recording()
                    if status.test:
                        filename = "rig_test"
                    else:
                        target_path = os.path.join(os.path.expanduser('~'),'completed',current_subjid)
                        if not os.path.exists(target_path):
                            mkdir_cmd = 'mkdir ' + target_path
                            os.system(mkdir_cmd)
                        move_cmd = "mv %s %s" % (filename,target_path)
                        print(move_cmd)
                        os.system(move_cmd)
                        subprocess.Popen("rsync -av --remove-source-files /home/delab/completed/ videofs:video/",shell=True)
                        filename =  "%s_%s_%s.mp4" % (status.current_msg['subjid'], str(status.current_msg['sessid']),status.current_msg['ts'])
                        filename = os.path.join(os.path.expanduser('~'),'in_process',filename)
                        current_subjid = status.current_msg['subjid']
                    mess.sessid = status.current_msg['sessid']
                    status.recording = True
                    # restart video recording with new filename
                    video_recording.init_recording(fname = filename,testing = status.test)
                    kill_video_flag = False
                    status.current_status = 'running'
                    # press shift to keep screen alive
                    try:
                        pyautogui.press('shift') #keep screen alive if received useful message
                    except:
                        print('error pressing shift')
                        traceback.print_exc()
                elif status.current_status == 'trial' and status.recording == True:
                    # have trial info, need to write to the caption file here
                    # press shift to keep screen alive
                    try:
                        pyautogui.press('shift') #keep screen alive if received useful message
                    except:
                        print('error pressing shift')
                        traceback.print_exc()
                else:
                    # wrong message format, ignoring/raise an exception
                    pass

            if filename == "rig_test" and status.current_status == 'running':
                elapsed = time.time() - t_start
                if elapsed > 10:
                    # only do 10s for rigtest, terminate rig_test video recording now
                    kill_video_flag = True
                    status.current_status = 'ended'


            # run opcv recording in condition
            if kill_video_flag:
                video_recording.destroy_recording()
                kill_video_flag = False
                status.recording = False

            video_recording.running_recording(status)

            #this thread have to be still alive, check if it's still alive?
            if not trig_mess.is_alive():
                #re-creating a new thread for message reciving to replace the expired one
                trig_mess=threading.Thread(target=mess.read_and_update_status,args=(status,),name='triggering_message')
                trig_mess.start()

    except KeyboardInterrupt:
        print('manually exiting')
        status.current_status = 'stop'
        mess.send_exit_message()#this will make the recv_from return and exit the threading
        mess.close_connection()
        if status.recording:
            # video recording is running
            video_recording.destroy_recording()

    except Exception as e: 
        print(e)
        status.current_status = 'stop'
        mess.send_exit_message()#this will make the recv_from return and exit the threading
        mess.close_connection()
        if status.recording:
            # video recording is running
            video_recording.destroy_recording()
        