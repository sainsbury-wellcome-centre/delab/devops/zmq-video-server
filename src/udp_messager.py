import socket
import json
from datetime import datetime
import traceback


class udp_messager(object):
    sock = '' 
    host_ip = ''
    host_port = 5888
    current_msg = ''
    msg = ''
    sessid = 0
    allow_reading = False #this act as a thread lock for read meassage. If not reading, then can enter reading
    
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.sock.settimeout(1200) #by deafult, timeout is 1200s

    def setNonBlocking(self):
        self.sock.setblocking(0)

    def setTimeout(self,tout):
        self.sock.setblocking(True)
        self.sock.settimeout(tout)
        
    def getConnection(self,addr,port):
        print("connecting with "+addr+" port "+port)
        self.host_ip = addr
        self.host_port = int(port)
        self.sock.bind((self.host_ip,self.host_port))
        self.allow_reading = True #allow reading after connection been established

    def read_message(self):
        while not self.allow_reading:
            pass  #not allow reading if another thread is reading, waiting here
        self.allow_reading = False
        try:
            buf, addr = self.sock.recvfrom(40960)
            self.current_msg = buf.decode()
        except socket.timeout:
            # trying to ignore the timeout error
            self.current_msg = None
        except:
            self.current_msg = None
            traceback.print_exc()
        self.allow_reading = True

    def send_exit_message(self):
        exiting_message = b"exiting"
        self.sock.sendto(exiting_message, (self.host_ip, self.host_port))

    def decode_message(self):
        self.msg = json.loads(self.current_msg)

    def read_and_update_status(self,status):
        self.read_message()
        if self.current_msg is None:
            status.updated = True
            status.current_status = None
        elif self.current_msg == "exiting":
            # exiting command to stop this thread, do nothing
            return
        else:
            try:
                # process real message
                self.decode_message()
                ts_str = self.msg['ts']
                ts_send = datetime.strptime(ts_str,'%Y_%m_%d_%H_%M_%S')
                now = datetime.now()
                difference = (now - ts_send).total_seconds()
                if difference<-1 or difference>5:
                    # got message from future? or message expired, do nothing but print
                    print("message expired or timezone not match, ignoring")
                    return
                status.current_status = self.msg['command']
                status.current_msg = self.msg
                status.updated = True
                status.test = (self.msg['test']== 'true')
                if (self.msg['command']== 'trial') and (self.msg['sessid'] != self.sessid and self.sessid>0) and status.recording and not status.test: #different sessid?
                    status.current_status = 'restart'
                elif (self.msg['command']== 'start') and (self.msg['sessid'] != self.sessid and self.sessid>0) and status.recording and not status.test: #different sessid?
                    status.current_status = 'restart'
                elif (self.msg['command']== 'trial') and status.recording == False and not status.test:
                    # got a trial command when recording was not started, start the recording
                    status.current_status = 'start'
            except:
                traceback.print_exc()
                print(f"incorrect message format: received message {self.current_msg}")    


    def close_connection(self):
        self.sock.close()
