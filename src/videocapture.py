import cv2 as cv


class video_recorder(object):
    isflip = False
    recording_filename = ''
    cap = ''
    out = ''
    fourcc = ''
    save = True
    recording_width = 640
    reocrding_height = 480
    saving_width = 640
    saving_height = 480
    saving_fps = 20.0

    def __init__(self,flip):
        self.recording_filename = "newfile.mp4"
        self.isflip = flip

    def init_recording(self,fname,testing=True):
        if ".mp4" not in fname:
            fname += ".mp4"
        self.recording_filename = fname
        self.save = not testing
        if not self.save:
            self.recording_filename = 'rig test'
        self.cap = cv.VideoCapture(0)
        cv.waitKey(1)
        if not self.cap.isOpened():
            print('cannot open camera with index 0, trying index -1')
            self.cap.release()
            self.cap = cv.VideoCapture(-1)
            cv.waitKey(1)
            if not self.cap.isOpened():
                raise Exception("camera cannot be opened, please check")
        self.cap.set(cv.CAP_PROP_FRAME_WIDTH, self.recording_width)
        self.cap.set(cv.CAP_PROP_FRAME_HEIGHT, self.reocrding_height)
        if self.save:
            self.fourcc = cv.VideoWriter_fourcc(*'mp4v')
            self.out = cv.VideoWriter(self.recording_filename, self.fourcc, self.saving_fps, (self.saving_width, self.saving_height))

    def destroy_recording(self):
        self.cap.release()
        if self.save:
            self.out.release()
        cv.destroyAllWindows()

    def running_recording(self,status):
        if status.recording == True:
            if self.cap.isOpened():
                ret, frame = self.cap.read()
                if self.isflip:
                    frame = cv.flip(frame, 0)
                if ret:
                    if self.save:
                        self.out.write(frame) 
                        overlay_text =  "%s SESSID: %s" % (status.current_msg['subjid'], str(status.current_msg['sessid']))  
                    else:
                        overlay_text = "rig test, closing in 10 secs"
                    frame = cv.putText(frame,overlay_text, (50, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv.LINE_4)
                    cv.imshow(self.recording_filename, frame)
                    
                cv.waitKey(30)


